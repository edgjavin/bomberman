/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bomberman;

import Ventanas.*;
import javafx.application.Application;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author Javier
 */
public class Juego extends Application{

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        VentanaPrincipal ventanaP=new VentanaPrincipal();
        Scene scene= new Scene(ventanaP.getRoot(), 550, 308);
        stage.setScene(scene);
        stage.show();
    }
    public static Stage getStage(Node n) {
        Stage stg = (Stage) n.getScene().getWindow();
        return stg;
    }
}
