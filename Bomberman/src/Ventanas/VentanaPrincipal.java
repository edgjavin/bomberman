/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ventanas;

import bomberman.Juego;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author Javier
 */
public class VentanaPrincipal {
    BorderPane root;
    VBox ButtonsPane;
    HBox labelPane;
    Label lbl_titulo;
    Button btn_juego;
    Button btn_instrucciones;
    Button btn_ranking;

    public VentanaPrincipal() {
        root=new BorderPane();
        ButtonsPane=new VBox();
        labelPane=new HBox();
        lbl_titulo=new Label("Bomberman");
        btn_juego=new Button("Nuevo Juego");
        btn_instrucciones=new Button("Instrucciones");
        btn_ranking=new Button("Ranking");
        setupPane();
        ButtonsPane.getChildren().addAll(btn_juego,btn_instrucciones,btn_ranking);
        labelPane.getChildren().add(lbl_titulo);
        root.setTop(labelPane);
        root.setCenter(ButtonsPane);
        setupButtons();
        root.getStylesheets().add("Estilos/VentanaP.css");
//        root.getStylesheets().add(VentanaPrincipal.class.getResource("VentanaP.css").toExternalForm());
    }
    void setupPane(){
        btn_juego.setId("btnJ");
        lbl_titulo.setId("lblT");
        labelPane.setAlignment(Pos.CENTER);
        lbl_titulo.setAlignment(Pos.CENTER);
        ButtonsPane.setSpacing(20);
        ButtonsPane.setAlignment(Pos.CENTER);
    }
    void setupButtons(){
        btn_juego.setOnAction((e)->{ 
            Stage stg= Juego.getStage(root);
            Scene s= new Scene((new VentanaJuego()).getRoot(),500,500 );
            stg.close();
            stg.setMaximized(true);
            stg.setScene(s);
            stg.show();
        });
       
    }
    
    public BorderPane getRoot() {
        return root;
    }
    
}
