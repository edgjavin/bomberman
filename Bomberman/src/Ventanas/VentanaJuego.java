/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ventanas;

import Juego.Bomberman;
import bomberman.Juego;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import static javafx.scene.input.KeyCode.DOWN;
import static javafx.scene.input.KeyCode.LEFT;
import static javafx.scene.input.KeyCode.RIGHT;
import static javafx.scene.input.KeyCode.UP;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Ellipse;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 *
 * @author Javier
 */
public class VentanaJuego {
    private VBox root;
    private Pane paneShape;
    private Label lbl_tiempo;
    Boolean termino=false;
    private Ellipse forma;
    
    public VentanaJuego() {
        root = new VBox();
        paneShape= new Pane();
        paneShape.setPrefSize(400, 600);
        Bomberman bomber=new Bomberman();
        forma=bomber.getForma();
        paneShape.getChildren().add(forma);
        lbl_tiempo=new Label("00:00");
        lbl_tiempo.setId("time");
        Button b= new Button("CERRAR PROGRAMA");
        b.setOnAction((e)->{
            System.exit(0);
        });
        root.getChildren().addAll(lbl_tiempo,paneShape,b);
        root.setFocusTraversable(true);
        Thread t_tiempo= new Thread(()->{
           while(termino==false){             
                
               Platform.runLater(new Runnable() {
                            @Override
                            public void run() {                                
                                animacionTiempo();
                            }
                        });
               try {
                       Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        Logger.getLogger(VentanaJuego.class.getName()).log(Level.SEVERE,null,e);
                    }
           } 
        }); 
        Thread t_bomber= new Thread(()->{
           while(termino==false){             
                try {
                       Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        Logger.getLogger(VentanaJuego.class.getName()).log(Level.SEVERE,null,e);
                    }
               Platform.runLater(new Runnable() {
                            @Override
                            public void run() { 
                                
                                animacionBomberman();
                            }
                        });
           } 
        }); 
        t_bomber.start();
//        
//         root.setOnKeyPressed((e)->{
//            switch (e.getCode()) {
//          case UP:    {
//              forma.setCenterY(forma.getCenterY() - 5);
//              
//          } break;
//          case RIGHT: {
//              forma.setCenterX(forma.getCenterX() + 5);
//              
//          }; break;
//          case DOWN: {
//              forma.setCenterY(forma.getCenterY() + 5);
//              
//          } break;
//           case LEFT:  {
//               forma.setCenterX(forma.getCenterX() - 5);
//               
//           } break;
//        }    
//        });           
       t_tiempo.start();
        root.getStylesheets().add("Estilos/VentanaJ.css");
    }
    void animacionTiempo(){
         final SimpleDateFormat cron=new SimpleDateFormat("mm:ss");
            Date now=new Date();
        try {
            now=cron.parse(lbl_tiempo.getText());
        } catch (ParseException ex) {
            Logger.getLogger(VentanaJuego.class.getName()).log(Level.SEVERE, null, ex);
        }
           Calendar calendar=  Calendar.getInstance();
           calendar.setTime(now);
            calendar.add(calendar.SECOND, +1);
            lbl_tiempo.setText(cron.format(calendar.getTime()));          
    }
    void animacionBomberman(){
        root.setOnKeyPressed((e)->{
           
                switch (e.getCode()) {
                
          case UP:    {
              if(!(forma.getCenterY()<=-10) ){
                  forma.setCenterY(forma.getCenterY() - 5);
              System.out.println(forma.getCenterY());
              }
              
          } break;
          case RIGHT: {
              if(!(forma.getCenterX()>=1025)){
                  forma.setCenterX(forma.getCenterX() + 5);
              System.out.println(forma.getCenterX());
              }
              
          }; break;
          case DOWN: {
              if(!(forma.getCenterY()>=500)){
                  forma.setCenterY(forma.getCenterY() + 5);
              System.out.println(forma.getCenterY());
              }
              
          } break;
           case LEFT:  {
               if(!(forma.getCenterX()<=-155)){
                   forma.setCenterX(forma.getCenterX() - 5);
               System.out.println(forma.getCenterX());
               }
               
           } break;
        }    
                
            
            
        }); 
    }     
//    }
//    Ellipse CreateBomberman(String bomber){
//        elipse=new Ellipse(70,95); 
//        Image img= new Image("/imagen/"+bomber+".png");
//        elipse.setFill(new ImagePattern(img));
//        elipse.setLayoutX(600);
//        elipse.setLayoutY(0);
//        paneshape.getChildren().add(elipse);
//        return elipse;
//    }
public VBox getRoot() {
        return root;
    }
    
}
