/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Juego;

import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Ellipse;

/**
 *
 * @author Javier
 */
public class Bomberman {
    private Image img;
    private Ellipse forma;


    public Bomberman() {
        forma= new Ellipse(50, 40);
        img= new Image("/imagenes/bomber.png");
        forma.setFill(new ImagePattern(img));
        forma.setLayoutX(200);
        forma.setLayoutY(50);
    }

    public Ellipse getForma() {
        return forma;
    }
    
            
}
